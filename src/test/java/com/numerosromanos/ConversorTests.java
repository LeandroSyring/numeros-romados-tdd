package com.numerosromanos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConversorTests {

    @Test
    public void testarConverterNumeroParaRomano() throws Exception {

        Conversor conversor = new Conversor();
        Assertions.assertEquals(conversor.converterNumeroParaRomano(10), "X");
        Assertions.assertThrows(Exception.class, () -> conversor.converterNumeroParaRomano(0));
        Assertions.assertThrows(Exception.class, () -> conversor.converterNumeroParaRomano(-1));
    }
}