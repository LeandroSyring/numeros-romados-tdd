package com.numerosromanos;

public class Numeros {

    private Integer numeroNormal;
    private String numeroRomano;

    public Numeros(Integer numeroNormal, String numeroRomano) {
        this.numeroNormal = numeroNormal;
        this.numeroRomano = numeroRomano;
    }

    public Integer getNumeroNormal() {
        return numeroNormal;
    }

    public void setNumeroNormal(Integer numeroNormal) {
        this.numeroNormal = numeroNormal;
    }

    public String getNumeroRomano() {
        return numeroRomano;
    }

    public void setNumeroRomano(String numeroRomano) {
        this.numeroRomano = numeroRomano;
    }
}
