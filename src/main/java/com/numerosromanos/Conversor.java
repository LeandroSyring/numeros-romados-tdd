package com.numerosromanos;

import java.util.ArrayList;
import java.util.List;
public class Conversor {

    public Conversor() {
    }

    public String converterNumeroParaRomano(Integer numeroNormal) throws Exception {

        if(numeroNormal <= 0){
            throw new Exception();
        }
        Dicionario dicionario = new Dicionario();

        for (Numeros numeros:dicionario.gerarDicionario()) {
            if(numeroNormal == numeros.getNumeroNormal()){
                return numeros.getNumeroRomano();
            }
        }
        return "Numero não encontrado";
    }
}
