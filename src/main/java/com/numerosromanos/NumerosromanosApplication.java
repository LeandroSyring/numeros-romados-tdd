package com.numerosromanos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class NumerosromanosApplication {

	public static void main(String[] args) {
		SpringApplication.run(NumerosromanosApplication.class, args);

		/*Conversor conversor = new Conversor();
		String teste = conversor.converterNumeroParaRomano(9);
		System.out.println(teste);*/

	}

}
