package com.numerosromanos;

import java.util.ArrayList;
import java.util.List;

public class Dicionario {

    public Dicionario() {
    }

    public List<Numeros> gerarDicionario(){

        List<Numeros> dicionario = new ArrayList<>();
        dicionario.add(new Numeros(1,"I"));
        dicionario.add(new Numeros(2,"II"));
        dicionario.add(new Numeros(3,"III"));
        dicionario.add(new Numeros(4,"IV"));
        dicionario.add(new Numeros(5,"V"));
        dicionario.add(new Numeros(6,"VI"));
        dicionario.add(new Numeros(7,"VII"));
        dicionario.add(new Numeros(8,"VIII"));
        dicionario.add(new Numeros(9,"IX"));
        dicionario.add(new Numeros(10,"X"));
        return dicionario;
    }




}
